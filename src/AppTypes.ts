export interface Item {
  id: number | null
  text: string | null
  done: boolean
}

export const enum EVENT {
  DONE,
  CHANGE,
  DELETE
}
